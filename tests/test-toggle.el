;;; tests/test-toggle.el -*- lexical-binding: t; -*-

;;; Copyright (C) 2021 Rose Kunkel

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU Affero General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option) any
;; later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
;; details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'buttercup)

(describe "The flowerline minor mode"
  :var ((old-mode-line-format '("An" "old" "format")))

  (before-each
    (setq mode-line-format old-mode-line-format))

  (it "restores the old mode-line format"
    (flowerline-mode)
    (flowerline-mode 0)
    (expect mode-line-format :to-equal old-mode-line-format))

  (it "is idempotent"
    (dotimes (_ 3)
      (flowerline-mode))
    (flowerline-mode 0)
    (expect mode-line-format :to-equal old-mode-line-format)

    (dotimes (_ 3)
      (flowerline-mode 0))
    (expect mode-line-format :to-equal old-mode-line-format))

  ;; TODO: how do I test this?
  (xit "restores mode-line window parameters"))

;;; tests/test-toggle.el ends here
