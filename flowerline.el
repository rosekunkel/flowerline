;;; flowerline.el --- A modular mode-line -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Rose Kunkel
;;
;; Author: Rose Kunkel <https://gitlab.com/rosekunkel>
;; Maintainer: Rose Kunkel <rose@rosekunkel.me>
;; Created: March 01, 2021
;; Modified: March 01, 2021
;; Version: 0.0.1
;; Keywords: mode-line faces
;; Homepage: https://gitlab.com/rosekunkel/flowerline
;; Package-Requires: ((emacs "27.1"))
;;
;; This file is not part of GNU Emacs.
;;
;; This program is free software; you can redistribute it and/or modify it under
;; the terms of the GNU Affero General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option) any
;; later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
;; details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Flowerline is a library for building modular mode-lines.
;;
;;; Code:

(defgroup flowerline nil
  "TODO"
  :group 'mode-line)

(defcustom flowerline-format '()
  "TODO"
  :type '(repeat sexp)
  :group 'flowerline)

(defvar flowerline--old-mode-line-format nil
  "TODO")

(defconst flowerline--mode-line-format
  '((:eval (flowerline-render-mode-line)))
  "TODO")

(cl-defgeneric flowerline-render (component)
  "Render a mode-line COMPONENT."
  (prin1-to-string component))

(defvar flowerline-formats (make-hash-table))

(defun flowerline-tag-windows ()
  (dolist (window (get-buffer-window-list))
    (map-put! flowerline-formats window (window-parameter window 'mode-line-format))
    (set-window-parameter window 'mode-line-format `((:eval (flowerline-render ,window)))))
  "")

(defun flowerline-untag-windows ()
  (map-do
   (lambda (window format)
     (set-window-parameter window 'mode-line-format format))
   flowerline-formats))

;; (defun flowerline-render (window)
;;   "TODO"
;;   (buffer-name (window-buffer window)))

(cl-defstruct flowerline-separator
  "A pair of mode-line components"
  separator left-color right-color invert)

(cl-defstruct flowerline-group
  "A group of mode-line components"
  sep-by
  components)

(cl-defstruct flowerline-accent
  face
  contents)

(cl-defmethod flowerline-render ((sep flowerline-separator))
  (flowerline-render
   (make-flowerline-accent
    :face `(:foreground ,(flowerline-separator-left-color sep)
            :background ,(flowerline-separator-right-color sep)
            :inverse-video ,(flowerline-separator-invert sep))
    :contents (flowerline-separator-separator sep))))

(cl-defmethod flowerline-render ((group flowerline-group))
  (string-join
   (seq-map #'flowerline-render (flowerline-group-components group))
   (flowerline-group-sep-by group)))

(cl-defmethod flowerline-render ((accent flowerline-accent))
  (pcase-let* (((cl-struct flowerline-accent face contents) accent)
               (str (flowerline-render contents)))
    (add-face-text-property 0 (seq-length str) face 'append str)
    str))

(cl-defmethod flowerline-render ((str string))
  "Render a string STR directly."
  str)

(defvar flowerline-rendered-mode-line nil)

(defun flowerline-render-mode-line ()
  "Render the complete mode-line to a string."
  (setq flowerline-rendered-mode-line
        (flowerline-render flowerline-format)))

;; '(modal-state (path vc) buffer-name fill major-mode)

(defun flowerline-enable ()
  "Enable flowerline."
  ;; (flowerline-tag-windows)
  (unless flowerline--old-mode-line-format
    (setq flowerline--old-mode-line-format mode-line-format))
  (setq mode-line-format flowerline--mode-line-format))

(defun flowerline-disable ()
  "Disable flowerline."
  ;; (flowerline-untag-windows)
  (when flowerline--old-mode-line-format
    (setq mode-line-format flowerline--old-mode-line-format))
  (setq flowerline--old-mode-line-format nil))


;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 
;; 


;;;###autoload
(define-minor-mode flowerline-mode
  "TODO: Documentation"
  :global t
  :lighter nil
  (setq flowerline-format
        (make-flowerline-accent
         :face '(:background "pink")
         :contents (make-flowerline-group
                    :sep-by ""
                    :components
                    (list
                     "text"
                     (make-flowerline-separator
                      :separator ""
                      :left-color "pink"
                      :right-color "blue")
                     (make-flowerline-accent
                      :face '(:background "blue"
                              :foreground "white")
                      :contents 'abc)
                     123))))
  (if flowerline-mode
      (flowerline-enable)
    (flowerline-disable)))

(provide 'flowerline)
;;; flowerline.el ends here
